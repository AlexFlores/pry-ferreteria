package Dao;

public interface LoginDao {

	public boolean emailRegistrado(String email);

	public boolean usuarioRegistrado(String usuario);

	public void registrarUsuario(String email, String usuario,String apePaterno,String apeMaterno,String password, String nombre);
}
