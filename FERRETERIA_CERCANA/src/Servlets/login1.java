package Servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.tribes.group.Response;

import Service.impl.LoginServiceImpl;
import Util.CifradoUtil;
import Util.validadorUtil;

/**
 * Servlet implementation class login1
 */
@WebServlet("/login1")
public class login1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String llaveCifrado = "Bc1LvpR9J8vFNFT9";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public login1() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("PRUEBA get::");
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("PRUEBA post::");
		String passwordCifrado = "";
		String usuario = request.getParameter("user");
		String password = request.getParameter("pass");
		validadorUtil val = new validadorUtil();
		LoginServiceImpl s = new LoginServiceImpl();

		passwordCifrado = llamada_cifrado_descifrado(password, "encrypt");

		RequestDispatcher res;
		if (request.getParameter("txtingresar") != null) {

			if (usuario.isEmpty() || password.isEmpty()) {
				request.setAttribute("v", "3");// Usuario no existe
			} else {
				if (s.usuarioExistente(usuario)) {
					if (passwordCifrado.equals("PAtSwPcHgaXoMzxGT3SCuQ==")) {

						request.setAttribute("v", "1");
						request.setAttribute("usuario", usuario);
					} else {

						request.setAttribute("v", "0");
					}
				} else {
					request.setAttribute("v", "2");// Usuario no existe
				}
			}

		}

		res = request.getRequestDispatcher("login.jsp");
		res.forward(request, response);
		// doGet(request, response);
	}

	public String llamada_cifrado_descifrado(String cadenaDatos, String tipo) {
		try {
			byte[] llave = llaveCifrado.getBytes("UTF-8");
			CifradoUtil cu = new CifradoUtil(llave);
			String salida_valor = "";
			if (tipo.equals("encrypt")) {
				salida_valor = cu.cifrarCadena(cadenaDatos);
			} else if (tipo.equals("decrypt")) {
				salida_valor = cu.decrypt(cadenaDatos);
			}
			return salida_valor;
		} catch (Exception e) {
			System.out.println("Error en el metodo llamada_cifrado_descifrado. " + e.toString());
		}
		return "";
	}

}
