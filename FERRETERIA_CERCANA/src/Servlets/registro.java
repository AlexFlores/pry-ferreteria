package Servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Service.impl.LoginServiceImpl;
import Util.validadorUtil;

/**
 * Servlet implementation class registro
 */
@WebServlet("/registro")
public class registro extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public registro() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		processRequest(request, response);
	}

	private void processRequest(HttpServletRequest request, HttpServletResponse response) {
		try {
			HttpSession session = request.getSession();
			String nombre = request.getParameter("txtNombre");
			String email = request.getParameter("txtEmail");
			String password = request.getParameter("pass1");
			String password1 = request.getParameter("pass2");
			String usuario = request.getParameter("txtUsuario");
			String apPaterno = request.getParameter("txtApPaterno");
			String apMaterno = request.getParameter("txtApMaterno");
			String passwordCifrado = "";
			login1 c = new login1();
			passwordCifrado = c.llamada_cifrado_descifrado(password, "encrypt");
			RequestDispatcher res;
			if (request.getParameter("txtRegitrar") != null) {
				//Pattern p = Pattern.compile("^([0-9a-zA-Z]([_.w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-w]*[0-9a-zA-Z].)+([a-zA-Z]{2,9}.)+[a-zA-Z]{2,3})$");
				Pattern p = Pattern.compile("^[_a-z0-9-]+(\\\\.[_a-z0-9-]+)*@\" +\r\n" + 
						"      \"[a-z0-9-]+(\\\\.[a-z0-9-]+)*(\\\\.[a-z]{2,4})$");
				System.out.println("EMAIL :: " + email);
				Matcher m = p.matcher(email);
				// validadorUtil val = new validadorUtil();
				LoginServiceImpl s = new LoginServiceImpl();

				// Validar campos vacios
				if (nombre.isEmpty() || email.isEmpty() || password.isEmpty() || password.isEmpty()
						|| password1.isEmpty() || usuario.isEmpty() || apPaterno.isEmpty() || apMaterno.isEmpty()) {
					session.setAttribute("error", "Hay campos vacios");
				} else {
					// se realiza las validaciones
					// no hay campos vacios valida que el email est� correctamente validada
					if (m.find()) {
						session.setAttribute("error", "La direccion de email no es correcta");
					} else {
						if (password.equals(password1)) {

							if (s.emailRegistrado(email)) {
								session.setAttribute("error", "Esta direccion de correo ya fue registrada");
							} else {
								if (s.usuarioExistente(usuario)) {
									session.setAttribute("error", "Ya existe un usuario con este nombre");
								} else {
									// Legado a este punto significa que todo esta correcto, por lo tanto ingreso a
									// la DB
									s.registrarUsuario(email, usuario, apPaterno, apMaterno, passwordCifrado, nombre);
									session.setAttribute("error", "1");
								}

							}

						} else {
							session.setAttribute("error", "Las contrase�as no son iguales");
						}
					}
				}

			}

			res = request.getRequestDispatcher("registro.jsp");

			res.forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
