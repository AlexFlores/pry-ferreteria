package Servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Service.impl.LoginServiceImpl;

/**
 * Servlet implementation class recuperarCuenta
 */
@WebServlet("/recuperarCuenta")
public class recuperarCuenta extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public recuperarCuenta() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}

	private void processRequest(HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		try {
			HttpSession session = request.getSession();
			String Correo = request.getParameter("txtEmail");
			LoginServiceImpl l = new LoginServiceImpl();
			RequestDispatcher res;

			if (request.getParameter("txtEnviar") != null) {
				if (!Correo.isEmpty()) {
					if (l.emailRegistrado(Correo)) {
						l.enviarCorreo(Correo);
						session.setAttribute("error", "1");
					} else {
						session.setAttribute("error", "Correo ingresado no existe");
					}
				} else {
					session.setAttribute("error", "Hay campos vacios");
				}

			}

			res = request.getRequestDispatcher("accountAcces.jsp");

			res.forward(request, response);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

}
