package Service;

public interface LoginService {

	public boolean emailRegistrado(String email);
	
	public boolean usuarioExistente(String usuario);
	
	public void registrarUsuario(String email,String usuario,String apePaterno,String apeMaterno, String password, String nombre);

	public void enviarCorreo(String email);
}
