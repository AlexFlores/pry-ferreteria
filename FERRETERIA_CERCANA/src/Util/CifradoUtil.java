package Util;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;


public class CifradoUtil {

	private byte[] key;

	private static final String ALGORITHM = "AES";

	public CifradoUtil(byte[] key) {
		this.key = key;
	}

	public byte[] encrypt(byte[] plainText) throws Exception {
		SecretKeySpec secretKey = new SecretKeySpec(key, ALGORITHM);
		Cipher cipher = Cipher.getInstance(ALGORITHM);
		cipher.init(Cipher.ENCRYPT_MODE, secretKey);

		return cipher.doFinal(plainText);
	}

	public String decrypt(String cadenaCifradaBase64) throws Exception {

		byte[] cipherText = new Base64().decode(cadenaCifradaBase64);
		SecretKeySpec secretKey = new SecretKeySpec(key, ALGORITHM);

		Cipher cipher = Cipher.getInstance(ALGORITHM);
		cipher.init(Cipher.DECRYPT_MODE, secretKey);

		return new String(cipher.doFinal(cipherText));
	}

	public String cifrarCadena(String cadena) throws Exception {
       System.out.println("ENTRA CIFRARCADENA"+ cadena);
		byte[] codigoCentralCifrado = this.encrypt(cadena.getBytes("UTF-8"));
	    System.out.println("ENTRA CIFRARCADENA ::::: "+ codigoCentralCifrado);
		String codigoCentralCifradoBase64 = new Base64().encodeToString(codigoCentralCifrado);
		return codigoCentralCifradoBase64;
	}
}
