<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page session="true"%>
<%
	HttpSession ses = request.getSession();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Ferreteria la Cercana</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<link rel="stylesheet" type="text/css" href="resources/css/menu.css">


</head>
<body>
	<nav class="navbar navbar-inverse"
		style="background-color: #ffc200; border-color: #ffc200">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#myNavbar">
					<span class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<tbody>
					<tr>
						<td class="m_7240958738099211080logo" align="center"
							style="padding: 46px 0 0 0"><img
							src="https://i.postimg.cc/HsJ5mHmb/logo-header-ferreteria.png"
							alt="Netflix" width="160" align="center"
							style="border: none; outline: none; border-style: none"
							class="CToWUd"></td>
					</tr>
				</tbody>
			</div>
			<div class="collapse navbar-collapse" id="myNavbar">
				<ul class="nav navbar-nav">
					<li class="active"><a href="menu.jsp?c=2">Home</a></li>
					<li class="dropdown"><a class="dropdown-toggle"
						data-toggle="dropdown" href="#">Page 1 <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="#">Page 1-1</a></li>
							<li class="divider"></li>
							<li><a href="#">Page 1-2</a></li>
							<li class="divider"></li>
							<li><a href="#">Page 1-3</a></li>
						</ul></li>
					<li class="dropdown"><a class="dropdown-toggle"
						data-toggle="dropdown" href="#">Page 2 <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="#">Page 1-1</a></li>
							<li class="divider"></li>
							<li><a href="#">Page 1-2</a></li>
							<li class="divider"></li>
							<li><a href="#">Page 1-3</a></li>
						</ul></li>
					<li><a href="#">Page 3</a></li>
					
				</ul>
				<ul class="nav navbar-nav navbar-right" >
				    <li class="active"><a class="glyphicon glyphicon-user" id="usuario" href="#"><span></span></a></li>
					<li><a href="menu.jsp?c=3"><span
							class="glyphicon glyphicon-lock"> Contraseņa</span></a></li>
					<li><a href="menu.jsp?c=1"><span
							class="glyphicon glyphicon-log-in"></span> Cerrar Sesion</a></li>
				</ul>
			</div>
		</div>
	</nav>

	<%	if (request.getParameter("c") != null) {
		if (request.getParameter("c").equals("2")) {%>
			<div class="container ferre">
				<jsp:include page="home.jsp" flush="true" />
			</div>
	<%}else if(request.getParameter("c").equals("3")){%>
			<div class="container ferre">
				<jsp:include page="updatePassword.jsp" flush="true" />
			</div>
		<%}
	}%>

	<script type="text/javascript">
<%if (ses.getAttribute("usu") != null) {
	//out.print("Bienvenido :: " + ses.getAttribute("usu"));%>
<%String usuario = (String) session.getAttribute("usu");%>
var  xUsuario = "<%=usuario%>";
document.getElementById("usuario").innerHTML =" Bienvenido "+ xUsuario.toString();

<%} else {
	response.sendRedirect("login.jsp");
}%>
	<%String permisos = (String) session.getAttribute("c");%>
	var  xpermiso = "<%=permisos%>";
		console.log('VALOR DE CERRAR ::' + xpermiso);
	<%if (request.getParameter("c") != null) {
	if (request.getParameter("c").equals("1")) {
		ses.invalidate();
		response.sendRedirect("login.jsp");
	}
}%>
		
	</script>
</body>
</html>


