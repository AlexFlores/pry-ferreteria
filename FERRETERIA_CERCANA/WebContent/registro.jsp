<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page session="true"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Registrarse</title>
<link rel="stylesheet" href="resources/css/estilo.css" />
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">

<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
	integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
	crossorigin="anonymous">

<link rel="stylesheet" type="text/css" href="resources/css/Login.css">

<script type="text/javascript">

</script>
</head>
<body>

	<div class="container ferre">
		<div class="d-flex justify-content-center h-100">
			<div class="card" id="card2">
				<button class="btn login_btn" onclick="location.href='login.jsp'"
					id="init" name="txtiniciarSesion">Iniciar Sesion</button>
				<div class="card-header">
					<h3>Registrarse</h3>
				</div>
				<div class="d-flex justify-content-end social_icon"></div>
				<div class="card-body">
					<form action="registro" method="post">
						<div class="input-group form-group">
							<div class="input-group-prepend"></div>
							<input type="text" name="txtNombre" class="form-control"
								placeholder="nombre">

						</div>
						<!-- BEGIN PRUEBA -->
						<div class="input-group form-group">
							<div class="input-group-prepend"></div>
							<input type="text" name="txtApPaterno" class="form-control"
								placeholder="Apellido paterno">

						</div>

						<div class="input-group form-group">
							<div class="input-group-prepend"></div>
							<input type="text" name="txtApMaterno" class="form-control"
								placeholder="Apellido materno">

						</div>

						<div class="input-group form-group">
							<div class="input-group-prepend"></div>
							<input type="text" name="txtUsuario" class="form-control"
								placeholder="Usuario">

						</div>
						<!-- END PRUEBA -->
						<div class="input-group form-group">
							<div class="input-group-prepend"></div>
							<input type="email" name="txtEmail" class="form-control"
								id="exampleInputEmail1" placeholder="email">

						</div>

						<div class="input-group form-group">
							<div class="input-group-prepend"></div>
							<input type="password" name="pass1" class="form-control"
								placeholder="contraseņa">
						</div>

						<div class="input-group form-group">
							<div class="input-group-prepend"></div>
							<input type="password" name="pass2" class="form-control"
								placeholder="repetir contraseņa">
						</div>

						<div class="form-group" id="nel">
							<input type="submit" name="txtRegitrar" value="Registrar"
								class="btn float-right login_btn">
						</div>
					</form>
				</div>

			</div>
		</div>
	</div>

	<script type="text/javascript">
		console.log("PRUEBA:: 11::");
		
		<%String permisos = (String) session.getAttribute("error");
			session.removeAttribute("error");
			if (permisos != null) {
				if (permisos.equals("1")) {%>
						alert("Registro Exitoso");
						<%
						} else {%>
						var  xpermiso = "<%=permisos%>";
					alert(xpermiso);
				<%}%>
					
				<%}%>
		
	</script>
</body>

</html>