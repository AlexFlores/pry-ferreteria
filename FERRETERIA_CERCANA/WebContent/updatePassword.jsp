<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Recuperar Contrase�a</title>
<link rel="stylesheet" type="text/css"
	href="resources/css/updatePassword.css">
</head>
<script type="text/javascript">
	
</script>
<body>
	<div class="container">
		<div class="card">
			<div class="card-header">
				<h3>Cambio de Contrase�a</h3>
			</div>
			<div class="">
				<form action="">
					<div class="input">
						<div class="input-group-prepend">
							<span class="glyphicon glyphicon-user"><i
								class="fas fa-user">Usuario</i></span>
						</div>
						<input type="text" name="user" class="form-control"
							placeholder="ingresar usuario">

					</div>
					<div class="input">
						<div class="input-group-prepend">
							<span class="glyphicon glyphicon-lock"><i
								class="fas fa-user">Contrase�a</i></span>
						</div>
						<input type="password" name="user" class="form-control"
							placeholder="ingresar contrase�a">

					</div>
					<div class="input">
						<div class="input-group-prepend">
							<span class="glyphicon glyphicon-lock"><i
								class="fas fa-user">Nueva contrase�a</i></span>
						</div>
						<input type="password" name="user" class="form-control"
							placeholder="ingresar nueva contrase�a">

					</div>
					<div class="input">
						<div class="input-group-prepend">
							<span class="glyphicon glyphicon-lock"><i
								class="fas fa-user">Repita su nueva contrase�a</i></span>
						</div>
						<input type="password" name="user" class="form-control"
							placeholder="Repetir nueva contrase�a">

					</div>

					<!-- <div class="input2">
						<div class="form-group row">
							<label for="inputEmail3" class="col-sm-2 col-form-label">repetir contrase�a</label>
							<div class="col-sm-10">
								<input type="email" class="form-control" id="inputEmail3"
									placeholder="Repetir nueva contrase�a">
							</div>
						</div>
					</div> -->
					<div class="input">
						<input type="submit" name="updatePassword" value="Cambiar contrase�a"
							class="btn float-right login_btn"> <input type="submit"
							name="cancelarUpdate" value="Cancelar" class="btn float-right login_btn">
					</div>
				</form>

			</div>
			<div class="card-footer">
				<div class="d-flex justify-content-center links">
					Tu contrase�a es importante!<br> Recuerda que siempre debe
					tener:<br> - M�nimo 8 caracteres. M�ximo 20 caracteres. <br>
					- Una May�scula y una min�scula.<br> - Un n�mero.<br> -
					No debe contener parte de tu nombre u apellidos.
				</div>
			</div>
		</div>
	</div>
</body>
</html>