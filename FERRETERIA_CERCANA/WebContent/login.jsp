<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page session="true"%>

<%
	HttpSession ses = request.getSession();
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>FERRETERYA TURYM</title>
<!-- <link rel="stylesheet" href="resources/css/bootstrap.css"/> -->

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">

<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
	integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
	crossorigin="anonymous">

<link rel="stylesheet" type="text/css" href="resources/css/Login.css">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>
<body>
<body>
	<div class="container ferre">
		<div class="d-flex justify-content-center h-100">
			<div class="card">
				<div class="card-header">
					<h3>Login</h3>
					<div class="d-flex justify-content-end social_icon">
						<a
							href="https://www.facebook.com/La-Ferreteria-Mayorista-100181048176086"
							target="_blank"> <span><i
								class="fab fa-facebook-square"></i></span>
						</a> <a
							href="https://accounts.google.com/signin/v2/identifier?hl=es-419&passive=true&continue=https%3A%2F%2Fwww.google.com%2Fsearch%3Fq%3Dgmail%26rlz%3D1C1GCEU_enPE891PE891%26oq%3Dgmail%26aqs%3Dchrome..69i57j69i61j69i60l2.905j0j1%26sourceid%3Dchrome%26ie%3DUTF-8&ec=GAZAAQ&flowName=GlifWebSignIn&flowEntry=ServiceLogin"
							target="_blank"><span><i
								class="fab fa-google-plus-square"></i></span> </a> <a
							href="https://twitter.com/" target="_blank"><span><i
								class="fab fa-twitter-square"></i></span></a>
					</div>
				</div>
				<div class="card-body">
					<form action="login1" method="post">
						<div class="input-group form-group">
							<div class="input-group-prepend">
								<span class="input-group-text"><i class="fas fa-user"></i></span>
							</div>
							<input type="text" name="user" class="form-control"
								placeholder="usuario">

						</div>
						<div class="input-group form-group">
							<div class="input-group-prepend">
								<span class="input-group-text"><i class="fas fa-key"></i></span>
							</div>
							<input type="password" name="pass" class="form-control"
								placeholder="contrase�a">
						</div>
						<div class="row align-items-center remember">
							<input type="checkbox">Recu�rdame
						</div>
						<div class="form-group">
							<input type="submit" name="txtingresar" value="Ingresar"
								class="btn float-right login_btn">
						</div>
					</form>
				</div>
				<div class="card-footer">
					<div class="d-flex justify-content-center links">
						�No tienes una cuenta?<a href="registro.jsp">Reg�strate</a>
					</div>
					<div class="d-flex justify-content-center">
						<a href="accountAcces.jsp">�Olvidaste tu contrase�a?</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		
	<%if (request.getAttribute("v") != null) {
	if (request.getAttribute("v").equals("1")) {
		ses.setAttribute("usu", request.getAttribute("usuario"));
		response.sendRedirect("menu.jsp");
	} else if (request.getAttribute("v").equals("0")) {
		session.removeAttribute("v");%>
		alert("Contrase�a Incorrecta");
	<%} else if (request.getAttribute("v").equals("2")) {
		session.removeAttribute("v");%>
		alert("Usuario ingresado no existe");
	<%} else if (request.getAttribute("v").equals("3")) {
		session.removeAttribute("v");%>
		alert("Ingrese valor, campos vacios");
	<%}
}%>
		
	</script>


</body>
</html>