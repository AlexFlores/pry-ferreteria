<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page session="true"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Recuperar Contrase�a</title>
<link rel="stylesheet" href="resources/css/estilo.css" />
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">

<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
	integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
	crossorigin="anonymous">

<link rel="stylesheet" type="text/css" href="resources/css/Login.css">
</head>
<body>
	<div class="container ferre">
		<div class="d-flex justify-content-center h-100">
			<div class="card">
				<div class="card-header">
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text" id="tam"><i
								class="fas fa-user"></i></span>
						</div>
						<h3>Recuperar Cuenta</h3>
					</div>
				</div>
				<div class="card-body">
					<form action="recuperarCuenta" method="post">
						<div class="input-group form-group">
							<a id="col">�Olvidaste tu contrase�a?</a> <a id="col2">Direcci�n
								de correo electr�nico que registro para iniciar sesi�n en su
								cuenta. Le enviaremos un correo electr�nico con su nueva
								contrase�a.</a>

						</div>
						<div class="input-group form-group">
							<div class="input-group-prepend">
								<span class="input-group-text"><i>@</i></span>
							</div>
							<input type="email" name="txtEmail" class="form-control"
								placeholder="email">
						</div>
						<div class="form-group">
							<input type="submit" name="txtEnviar" value="env�ar"
								class="btn float-right login_btn" id="initi">
						</div>
					</form>
				</div>
				<div class="card-footer">
					<div class="d-flex justify-content-center">
						<a href="login.jsp">Acceso a la Cuenta</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		console.log("PRUEBA:: 11::");
		
		<%String permisos = (String) session.getAttribute("error");
			session.removeAttribute("error");
			if (permisos != null) {
				if (permisos.equals("1")) {%>
						alert("Se env�o la nueva contrase�a a su bandeja de correo, favor de validar");
						<%
						} else {%>
						var  xpermiso = "<%=permisos%>";
					alert(xpermiso);
				<%}%>
					
				<%}%>
		
	</script>
</body>
</html>